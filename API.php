<?php
  require_once('MysqliDb.php');
  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
  header("Access-Control-Allow-Headers: Content-Type");

  class API {
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'ten9db');
    }

    /**
    * HTTP GET Request
    *
    * @param $payload
    */
    public function httpGet($payload)
    {
        if (!is_array($payload) || !isset($payload['id'])) {
            $response = [
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Invalid payload. Expected an array with an "id" key.'
            ];
            http_response_code(400); // Bad Request
            return json_encode($response);
        }

        $id = $payload['id'];

        $result = $this->db->where('id', $id)->getOne('information');

        if (empty($result)) {
            // No data found for the provided ID
            $response = [
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'No data found for the provided ID'
            ];

            return json_encode($response);
        } else {
            // Since $result might be an array of arrays, access the first row
            $row = $result;

            // Extract data from the row
            $extractedData = [
                'id' => $id,
                'first_name' => $row['first_name'],
                'middle_name' => $row['middle_name'],
                'last_name' => $row['last_name'],
                'contact_number' => $row['contact_number']
            ];

            // Return a success response with the extracted data
            $response = [
                'method' => 'GET',
                'status' => 'success',
                'data' => $extractedData
            ];
        }

        return json_encode($response);
    }

    /**
    * HTTP POST Request
    *
    * @param $payload
    */
    public function httpPost($payload) {
        if (is_array($payload) && !empty($payload)) {

            $toCheck = array(
                $payload['first_name'],
                $payload['middle_name'],
                $payload['last_name'],
            );

            $hasIntegerProperty = false;
            foreach ($toCheck as $property => $value) {
                if (is_int($value)) {
                    $hasIntegerProperty = true;
                    break; // Exit the loop if an integer property is found
                }
            };

            if (!$hasIntegerProperty){
                // $payload is an array and not empty and every property is not an int
                $result = $this->db->insert('information', $payload);

                if ($result) {
                    // Insert query succeeded
                    $getInserted = $this->db->where('id', $result)->getOne('information');

                    $insertedDataArray = [
                        'id' => $getInserted['id'],
                        'first_name' => $getInserted['first_name'],
                        'middle_name' => $getInserted['middle_name'],
                        'last_name' => $getInserted['last_name'],
                        'contact_number' => $getInserted['contact_number']
                    ];
                    // Return a success response
                    $response = [
                        'method' => 'POST',
                        'status' => 'success',
                        'data' => $insertedDataArray
                    ];
                    http_response_code(400);
                    return json_encode($response);
                } else {
                    // Insert query failed
                    $response = [
                        'method' => 'POST',
                        'status' => 'failed',
                        'message' => 'Insert Failed.'
                    ];
                    http_response_code(400);
                    return json_encode($response);
                }

            } else{
                $response = [
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Contact number must be the only integer in the payload.'
                ];
                http_response_code(400);
                return json_encode($response);
            }

        } else {
            // $payload is not an array or empty
            $response = [
                'error' => 'Invalid payload. Expected a non-empty array.'
            ];
            http_response_code(400);
            return json_encode($response);
        }
    }

    /**
    * HTTP PUT Request
    *
    * @param $id
    * @param $payload
    */
    public function httpPut($id, $payload)
    {
        // $id = (int) $_GET['id'];
        
        if (!empty($id) && !empty($payload)) {
            
            // Check if the id passed in matches the id in the payload
            if (!empty($payload['id']) && $payload['id'] === $id) {
                $this->db->where('id', $id);

                // Execute a query to update data in the database
                $result = $this->db->update('information', $payload);

                if ($result) {
                    // Fetch the updated data from the database
                    $updatedData = $this->db->where('id', $id)->getOne('information');

                    // Return a success response with the updated data
                    $response = [
                        'status' => 'success',
                        'data' => $updatedData,
                        'method' => 'PUT'
                    ];
                    // echo $updatedData[0];
                    return json_encode($response, JSON_PRETTY_PRINT);
                } else {
                    // Return a failed response
                    $response = [
                        'status' => 'failed',
                        'message' => 'Failed to Update Data',
                        'method' => 'PUT'
                    ];
                    return json_encode($response, JSON_PRETTY_PRINT);
                }
            } else {
                // ID mismatch between parameter and payload
                $response = [
                    'status' => 'failed',
                    'message' => 'Failed to Update Data',
                    'method' => 'PUT',
                    'error' => 'ID mismatch between parameter and payload'
                ];
                http_response_code(400); // Bad Request
                return json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            // Invalid ID or empty payload
            $response = [
                'error' => 'Invalid ID or empty payload'
            ];
            http_response_code(400); // Bad Request
            return json_encode($response, JSON_PRETTY_PRINT);
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param int $id The ID of the data to be deleted
     * @param mixed $payload The payload containing deletion data (ID or array of IDs)
     */
    public function httpDelete($id, $payload)
    {
        // $id = (int) $_GET['id'];

        if (!empty($id) && !empty($payload)) {
            // Check if the id passed in matches the id in the payload
            if (is_array($payload)) {
                if (in_array($id, $payload)) {
                    $this->db->where('id', $payload, 'IN');
                } else {
                    $response = [
                        'status' => 'failed',
                        'message' => 'Failed',
                        'method' => 'DELETE',
                    ];
                    http_response_code(400);
                    return json_encode($response, JSON_PRETTY_PRINT);
                }
            } else {
                // Use the where() function to specify the ID in the WHERE clause condition
                $this->db->where('id', $id);
            }

            // Delete data from the database
            $result = $this->db->delete('information');

            if ($result) {
                // Return a success response
                $response = [
                    'status' => 'success',
                    'message' => 'Data Deleted Successfully',
                    'method' => 'DELETE'
                ];
                return json_encode($response, JSON_PRETTY_PRINT);
            } else {
                // Return a failed response
                $response = [
                    'status' => 'failed',
                    'message' => 'Failed to Delete Data',
                    'method' => 'DELETE'
                ];
                return json_encode($response, JSON_PRETTY_PRINT);
            }
        } else {
            // Invalid id or empty payload
            $response = [
                'status' => 'failed',
                'message' => 'Failed to Delete Data',
                'method' => 'DELETE'
            ];
            http_response_code(400); // Bad Request
            return json_encode($response, JSON_PRETTY_PRINT);
        }
    }
    
}

    // //Identifier if what type of request
    // $request_method = $_SERVER['REQUEST_METHOD'];

    // // For GET,POST,PUT & DELETE Request
    // if ($request_method === 'GET') {
    //     $received_data = $_GET;
    // } else {
    //     //check if method is PUT or DELETE, and get the ids on URL
    //     if ($request_method === 'PUT' || $request_method === 'DELETE') {
    //         $request_uri = $_SERVER['REQUEST_URI'];


    //         $ids = null;
    //         $exploded_request_uri = array_values(explode("/", $request_uri));


    //         $last_index = count($exploded_request_uri) - 1;


    //         $ids = $exploded_request_uri[$last_index];


    //         }
    //     }

    // //payload data
    // $received_data = json_decode(file_get_contents('php://input'), true);

    // $api = new API;


    // //Checking if what type of request and designating to specific functions
    // switch ($request_method) {
    //      case 'GET':
    //          $api->httpGet($received_data);
    //          break;
    //      case 'POST':
    //          $api->httpPost($received_data);
    //          break;
    //      case 'PUT':
    //          $api->httpPut($ids, $received_data);
    //          break;
    //      case 'DELETE':
    //          $api->httpDelete($ids, $received_data);
    //          break;
    // }
   

?>