<?php

require_once 'API.php';
use PHPUnit\Framework\TestCase;


class APITest extends TestCase {
    protected function setUp(): void
    {
        $this->api = new API();
    }

    // *** SUCCESSTESTS ***
    public function testHttpGet()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array('id' => 12);
        $result = json_decode($this->api->httpGet($payload), true);
        $expectedKeys = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'contact_number'
        ];
        
        $this->assertEquals($expectedKeys, array_keys($result['data']));
        
        $this->assertNotEmpty($result['data']['id']);
        // $this->assertNotEmpty($result['data']['document_id']);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpPost()
    {
        $_SERVER['REQUEST_METHOD'] = 'POST';

        $payload = array(
            'first_name' => 'Test',
            'middle_name' => 'test',
            'last_name' => 'last test',
            'contact_number' => 654655
        );
        $result = json_decode($this->api->httpPost($payload), true);
        $expectedKeys = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'contact_number'
        ];
        
        $this->assertEquals($expectedKeys, array_keys($result['data']));
        
        $this->assertNotEmpty($result['data']['id']);
        // $this->assertNotEmpty($result['data']['document_id']);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'success');
        $this->assertArrayHasKey('data', $result);
    }

    public function testHttpPut()
    {
        $_SERVER['REQUEST_METHOD'] = 'PUT';

        $payload = array(
            'id' => 12,
            'first_name' => 'Updated Name',
            'middle_name' => 'updated',
            'last_name' => 'Updated Last',
            'contact_number' => 987654321
        );
        $result = json_decode($this->api->httpPut(12, $payload), true);
        $expectedKeys = [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'contact_number'
        ];
        $status = $result['status'];

        // Check if the $result data in not empty and status is equal to success
        $this->assertArrayHasKey('data', $result);
        $this->assertEquals($result['status'], 'success');

        $this->assertEquals($expectedKeys, array_keys($result['data']));
    }

    public function testHttpDelete()
    {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $payload = array('id' => 16);
        $result = json_decode($this->api->httpDelete(16, $payload), true);
        $expectedKeys = [
            'status',
            'message',
            'method'
        ];
        $this->assertEquals($expectedKeys, array_keys($result));
        $this->assertNotEmpty($result['status']);
        $this->assertArrayHasKey('status', $result);
        $this->assertNotEquals($result['status'], 'failed');
    }

    // *** FAILEDTEST ***
    public function testHttpGet() {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        $payload = array('id' => null);
        $result = json_decode($this->api->httpGet($payload), true);
        $expectedKeys = [
            'method',
            'status',
            'message'
        ];

        echo 'GET request result: ' . "\r\n" .
            'Status: ' . $result['status'] . "\r\n" .
            'Message: ' . $result['message'] . "\r\n" .
            'Method: ' . $result['method']. "\r\n\r\n" ;

        $this->assertEquals($expectedKeys, array_keys($result));
        
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
    }

    public function testHttpPost() {
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $payload = array(
            'first_name' => 1,
            'middle_name' => 2,
            'last_name' => 3,
            'contact_number' => 987654321
        );

        $result = json_decode($this->api->httpPost($payload), true);
        echo 'POST request result: ' . "\r\n" .
            'Status: ' . $result['status'] . "\r\n" .
            'Message: ' . $result['message'] . "\r\n" .
            'Method: ' . $result['method']. "\r\n\r\n" ;

        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }

    public function testHttpPut() {
        $_SERVER['REQUEST_METHOD'] = 'PUT';
        $payload = array(
            'id' => 12,
            'first_name' => 'Updated Name',
            'middle_name' => 'updated',
            'last_name' => 'Updated Last',
            'contact_number' => 987654321
        );
        $result = json_decode($this->api->httpPut(13, $payload), true);

        echo 'PUT request result: ' . "\r\n" .
            'Status: ' . $result['status'] . "\r\n" .
            'Message: ' . $result['message'] . "\r\n" .
            'Method: ' . $result['method']. "\r\n\r\n" ;

        $this->assertNotEmpty($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
        $this->assertArrayHasKey('message', $result);
    }

    public function testHttpDelete() {
        $_SERVER['REQUEST_METHOD'] = 'DELETE';

        $payload = array('id' => null);
        $result = json_decode($this->api->httpDelete(16, $payload), true);
        $expectedKeys = [
            'status',
            'message',
            'method'
        ];

        echo 'Delete request result: ' . "\r\n" .
            'Status: ' . $result['status'] . "\r\n" .
            'Message: ' . $result['message'] . "\r\n" .
            'Method: ' . $result['method'] . "\r\n\r\n";

        $this->assertEquals($expectedKeys, array_keys($result));
        $this->assertNotEmpty($result['status']);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals($result['status'], 'failed');
    }
}